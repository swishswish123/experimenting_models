import os
import sys
import time

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from tensorflow.keras.preprocessing.image import img_to_array, array_to_img

from tensorflow.keras import Input
from tensorflow.keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16

from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input

from tensorflow.keras.optimizers import Adam

# import pickle
import seaborn as sns

# for multiprocessing
import concurrent.futures
from itertools import repeat


# ---------------- PREPROCESSING----------------------------

def preprocessing(train_images, test_images):
    # 1. making the scale 0-1
    # train_images = train_images / 255.0
    # test_images = test_images / 255.0

    # 2. resizing images so we can use them with VGG model
    train_images = np.stack([train_images] * 3, axis=3)
    test_images = np.stack([test_images] * 3, axis=3)

    # 2. resizing images so we can use them with VGG model
    # train_images = np.dstack([train_images]*3)
    # test_images = np.dstack([test_images]*3)

    # reshaping to tensor format
    train_images = train_images.reshape([-1, 28, 28, 3])
    test_images = test_images.reshape([-1, 28, 28, 3])

    # resizing images to (32,32,3) instead of (28,28,3)
    train_images = np.asarray(
        [img_to_array(array_to_img(im, scale=False).resize((32, 32))) for im in train_images]
    )

    test_images = np.asarray(
        [img_to_array(array_to_img(im, scale=False).resize((32, 32))) for im in test_images]
    )

    return train_images, test_images


# ---------------- CREATE MODEL----------------------------

def create_model(file_name):
    ####### to investigate input_tensor
    # input_tensor = Input(shape=(32,32,3))
    if file_name == 'vgg':
        vgg_model = VGG16(
            include_top=True,
            weights=None,
            input_shape=(32, 32, 3),
            pooling=None,
            classes=10,
            # classifier_activation=None,
            classifier_activation='softmax'
        )

        model = Model(vgg_model.inputs, vgg_model.outputs)

        print('model created')
        return model
    elif file_name == 'resnet':

        resnet_model = tf.keras.applications.ResNet50(
            include_top=True,
            weights=None,
            input_shape=(32, 32, 3),
            pooling=None,
            classes=10,
            # classifier_activation=None,
            classifier_activation='softmax'
        )

        model = Model(resnet_model.inputs, resnet_model.outputs)

        print('model created')
        return model
    elif file_name == 'normal':
        model = tf.keras.Sequential([
            tf.keras.layers.Flatten(input_shape=(32, 32, 3)),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.Dense(10),
            tf.keras.layers.Softmax()
        ])
        print('model created')
        return model
    else:
        print('file name not found- model name not correct')


# ---------------- COMPILE MODEL----------------------------

def weighted_categorical_crossentropy(class_weights):
    """
    A weighted version of keras.objectives.categorical_crossentropy
    Variables:
        class_weights: numpy array of shape (C,) where C is the number of classes
    """
    class_weights = tf.keras.backend.variable(class_weights)

    def loss(y_true, y_pred):
        y_true_uint8 = tf.convert_to_tensor(y_true, dtype=tf.uint8)
        y_true = tf.image.convert_image_dtype(y_true_uint8, dtype=tf.float32)

        # scale predictions so that the class probas of each sample sum to 1
        y_pred /= tf.keras.backend.sum(y_pred, axis=-1, keepdims=True)
        # clip to prevent NaN's and Inf's
        y_pred = tf.keras.backend.clip(y_pred, tf.keras.backend.epsilon(), 1 - tf.keras.backend.epsilon())
        # calc
        print('y_true: ', y_true)
        print('tf.keras.backend.log(y_pred): ', tf.keras.backend.log(y_pred))
        print('class_weights: ',class_weights)

        print('----------------------------TYPES----------------------------')
        print('y_true: ', type(y_true[0]))
        print('tf.keras.backend.log(y_pred): ', type(tf.keras.backend.log(y_pred)[0]))
        print('class_weights: ',type(class_weights[0]))

        loss = y_true * tf.keras.backend.log(y_pred) * class_weights
        loss = -tf.keras.backend.sum(loss, -1)
        return loss

    return loss


def compile_model(method, learning_rate, model):

    # defining loss function
    if method == 'weighted_loss':
        class_weights = np.zeros(10) + 1
        loss_func = weighted_categorical_crossentropy(class_weights)
    else:
        loss_func= tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    # choosing optimiser with specified learning rate
    optimiser_01 = Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimiser_01, loss=loss_func, metrics=['accuracy'])
    print('model compiled with learning rate', learning_rate)


# ---------------- TRAIN MODEL----------------------------

def train_model(train_images, train_labels, test_images, test_labels, model, batch_size, epochs):
    history = model.fit(train_images, train_labels,
                        epochs=epochs, batch_size=batch_size,
                        validation_data=(test_images, test_labels))

    return history


# ---------------- PLOTTING RESULTS----------------------------

def plot_results(file_name, history, epochs, learning_rate, batch_size):
    # extracting accuracies and loss from history
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure()
    plt.title(f'epochs {epochs}, lr {learning_rate}, bs {batch_size}')

    # plotting accuracies
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Accuracy')
    plt.plot(epochs_range, val_acc, label='validation Accuracy')
    plt.xlabel('epochs')
    plt.ylabel('accuracy')
    plt.legend()

    # plotting losses
    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Loss')
    plt.plot(epochs_range, val_loss, label='validation Loss')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.savefig(f'{file_name}/plots/figure_E_{epochs}_LR_{learning_rate}_BS_{batch_size}.png')


def predict_img(test_img, model):
    test_img = tf.expand_dims(test_img, 0)
    prediction = model.predict(test_img)
    prediction = np.argmax(prediction) # selecting class with highest probability
    #predictions.append(prediction)
    # generating heatmaps
    #if last_conv_layer_name != None:
        #i += 1
        #heatmap = make_gradcam_heatmap(test_img, model, last_conv_layer_name, pred_index=None)
        #heat_maps.append(heatmap)
        # generating heatmap-image superimposed
        #if i == 1 or i == 10 or i == 100:
        #    print('this is an example prediction: ', prediction)
        #    img_name = f"cam_{i}.jpg"
        #    alpha = 0.4
        #    generate_heatmap_image(test_img, heatmap, img_name, alpha, file_name)
    return prediction

def generate_heatmap(test_img, model, last_conv_layer_name):
    if last_conv_layer_name != None:
        heatmap = make_gradcam_heatmap(test_img, model, last_conv_layer_name, pred_index=None)
# ---------------- MAKING PREDICTIONS ----------------------------
def inference(method, model, test_images, last_conv_layer_name, file_name):
    '''
    tic_mul = time.perf_counter()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        predictions_mul = list(executor.map(predict_img, test_images, repeat(model)))
        heatmaps_mul =list(executor.map(generate_heatmap,test_images, repeat(model), repeat(last_conv_layer_name)))
    toc_mul = time.perf_counter()

    print(f'performance of multiprocess: {toc_mul-tic_mul} sec' )
    '''

    tic_normal = time.perf_counter()
    # predict with model label of test data
    predictions = []
    heat_maps = []
    i = 1
    for test_img in test_images:
        test_img = tf.expand_dims(test_img, 0)
        prediction = model.predict(test_img)
        prediction = np.argmax(prediction) # selecting class with highest probability
        predictions.append(prediction)
        # generating heatmaps
        if last_conv_layer_name != None:
            i += 1
            heatmap = make_gradcam_heatmap(test_img, model, last_conv_layer_name, pred_index=None)
            heat_maps.append(heatmap)
            # generating heatmap-image superimposed
            if i == 1 or i == 10 or i == 100:
                print('this is an example prediction: ', prediction)
                img_name = f"cam_{i}.jpg"
                alpha = 0.4
                generate_heatmap_image(test_img, heatmap, img_name, alpha, file_name)
    toc_normal = time.perf_counter()

    print(f'performance of slow: {toc_normal-tic_normal} sec' )
    '''
    pred_n = str(predictions)
    pred_m = str(predictions_mul)

    hm_n = str(heat_maps)
    hm_m = str(heatmaps_mul)
    if pred_n == pred_m:
        print ("Predictions are identical")
    else :
        print ("Predictions different: ", pred_n, pred_m)

    if hm_n == hm_m:
        print ("HEATMPAS are identical")
    else :
        print ("heatmaps different: ", hm_n, hm_m)
    '''

    return predictions, heat_maps


def generate_heatmap_image(test_img, heatmap, img_name, alpha, file_name):
    # Convert image to array to be able to do processing
    img = tf.keras.preprocessing.image.img_to_array(test_img[0])

    # Rescale heatmap to a range 0-255
    htmp = np.uint8(255 * heatmap)

    # Use jet colormap to colorize heatmap
    jet = cm.get_cmap("jet")
    # Use RGB values of the colormap
    jet_colors = jet(np.arange(256))[:, :3]
    jet_heatmap = jet_colors[htmp]

    # Create an image with RGB colorized heatmap
    jet_heatmap = tf.keras.preprocessing.image.array_to_img(jet_heatmap)
    jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
    jet_heatmap = tf.keras.preprocessing.image.img_to_array(jet_heatmap)

    # Superimpose the heatmap on original image
    superimposed_img = jet_heatmap * alpha + img
    superimposed_img = tf.keras.preprocessing.image.array_to_img(superimposed_img)

    # Save the superimposed image
    superimposed_img.save(f'{file_name}/heatmap_examples/{img_name}')


# ---------------- PLOTTING CONFUSION MATRIX----------------------------
def plot_confusion_mat(method, file_name, epochs, learning_rate, batch_size, predictions, test_labels):
    # calculating values for confusion matrix
    print(predictions)
    if method == 'weighted_loss':
        confusion_mtx = tf.math.confusion_matrix(test_labels, predictions[0])
    else:
        confusion_mtx = tf.math.confusion_matrix(test_labels, predictions)
    # generating confusion matrix figure
    plt.figure(figsize=(10, 8))
    sns.heatmap(confusion_mtx, annot=True, fmt='g')
    plt.xlabel('Predicted label')
    plt.ylabel('True label')
    # saving confusion matrix
    plt.savefig(f'{file_name}/confusion_plots/confusion_E_{epochs}_LR_{learning_rate}_BS_{batch_size}.png')


def make_gradcam_heatmap(img_array, model, last_conv_layer_name, pred_index=None):
    # First, we create a model that maps the input image to the activations
    # of the last conv layer as well as the output predictions
    grad_model = tf.keras.models.Model(
        [model.inputs], [model.get_layer(last_conv_layer_name).output, model.output]
    )

    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        last_conv_layer_output, preds = grad_model(img_array)
        if pred_index is None:
            pred_index = tf.argmax(preds[0])
        class_channel = preds[:, pred_index]

    # This is the gradient of the output neuron (top predicted or chosen)
    # with regard to the output feature map of the last conv layer
    grads = tape.gradient(class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))
    # pooled_grads = tf.reduce_mean(grads, axis=None)

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    # then sum all the channels to obtain the heatmap class activation
    last_conv_layer_output = last_conv_layer_output[0]
    heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
    heatmap = tf.squeeze(heatmap)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
    return heatmap.numpy()


def get_model_memory_usage(batch_size, model):
    try:
        from keras import backend as K
    except:
        from tensorflow.keras import backend as K
    shapes_mem_count = 0
    internal_model_mem_count = 0
    for l in model.layers:
        layer_type = l.__class__.__name__
    if layer_type == 'Model':
        internal_model_mem_count += get_model_memory_usage(batch_size, l)
    single_layer_mem = 1
    out_shape = l.output_shape
    if type(out_shape) is list:
        out_shape = out_shape[0]
    for s in out_shape:
        if s is None:
            continue
    single_layer_mem *= s
    shapes_mem_count += single_layer_mem
    trainable_count = np.sum([K.count_params(p) for p in model.trainable_weights])
    non_trainable_count = np.sum([K.count_params(p) for p in model.non_trainable_weights])
    number_size = 4.0
    if K.floatx() == 'float16':
        number_size = 2.0
    if K.floatx() == 'float64':
        number_size = 8.0
    total_memory = number_size * (batch_size * shapes_mem_count + trainable_count + non_trainable_count)
    gbytes = np.round(total_memory / (1024.0 ** 3), 3) + internal_model_mem_count
    return gbytes


def main(file_name, epochs, learning_rate, batch_size):

    #method = 'weighted_loss'
    method = 'weighted_loss'
    print('######## LEARNING RATE ', learning_rate, ' BATCH SIZE', batch_size, ' and', epochs, ' EPOCHS')

    # ----------------   CREATING MODEL  ----------------------------

    model = create_model(file_name)
    compile_model(method,learning_rate, model)
    # model.summary()
    memory_usage = get_model_memory_usage(batch_size, model)

    print(f'Memory usage of model: {memory_usage}GB')

    # ---------------- CREATING FOLDER WHERE TO SAVE DATA----------------------------

    path = os.getcwd()  # getting current path

    # creating folder called resnet with subfolders histories (for history files) and plots (for all figures)
    if not os.path.exists(f'{path}/{file_name}'):
        dir_name = f"{path}/{file_name}/histories"
        os.makedirs(dir_name)
        dir_name = f"{path}/{file_name}/plots"
        os.mkdir(dir_name)
        dir_name = f"{path}/{file_name}/confusion_plots"
        os.mkdir(dir_name)
        dir_name = f"{path}/{file_name}/heatmap_examples"
        os.mkdir(dir_name)
        dir_name = f"{path}/{file_name}/predictions"
        os.mkdir(dir_name)

    # ---------------- LOADING DATA----------------------------

    # loading clothes images
    fashion_mnist = tf.keras.datasets.fashion_mnist

    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

    # names of classes:
    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                   'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

    # ---------------- PRE-PROCESSING DATA----------------------------

    train_images, test_images = preprocessing(train_images, test_images)
    #train_labels=train_labels.view('<f4')
    #test_labels=test_labels.view('<f4')
    # ---------------- TRAINING MODEL ----------------------------

    history = train_model(train_images, train_labels, test_images, test_labels, model, batch_size, epochs)

    # saving history file

    # with open(f'{file_name}/histories/history_E_{epochs}_LR_{learning_rate}_BS_{batch_size}', 'wb') as file_pi:
    #    pickle.dump(history.history, file_pi)

    np.save(f'{file_name}/histories/history_E_{epochs}_LR_{learning_rate}_BS_{batch_size}', history.history)

    plot_results(file_name, history, epochs, learning_rate, batch_size)

    # ---------------- HEATMAPS AND PREDICTIONS ----------------------------

    if file_name == 'resnet':
        last_conv_layer_name = "conv5_block3_3_conv"
    elif file_name == 'vgg':
        last_conv_layer_name = 'block5_conv3'
    elif file_name == 'normal':
        last_conv_layer_name = None
    predictions = inference(method, model, test_images, last_conv_layer_name, file_name)
    np.save(f'{file_name}/predictions/predictions_E_{epochs}_LR_{learning_rate}_BS_{batch_size}', history.history)

    print('PLOTTING CONFUSION MATRIX')
    plot_confusion_mat(method, file_name, epochs, learning_rate, batch_size, predictions, test_labels)
