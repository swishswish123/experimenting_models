import os
import sys

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from IPython.display import Image, display
import matplotlib.cm as cm

from tensorflow.keras.preprocessing.image import img_to_array, array_to_img

from tensorflow.keras import Input
from tensorflow.keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16

from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input

from tensorflow.keras.optimizers import Adam

import pickle

import seaborn as sns

from common_functions import main

'''
# ---------------- PREPROCESSING----------------------------

def preprocessing(train_images, test_images):
    # 1. making the scale 0-1
    train_images = train_images / 255.0
    test_images = test_images / 255.0


    # 2. resizing images so we can use them with VGG model
    train_images = np.dstack([train_images] * 3)
    test_images = np.dstack([test_images] * 3)

    # reshaping to tensor format
    train_images = train_images.reshape([-1, 28, 28, 3])
    test_images = test_images.reshape([-1, 28, 28, 3])

    train_images = np.asarray(
        [img_to_array(array_to_img(im, scale=False).resize((32, 32))) for im in train_images]
    )

    test_images = np.asarray(
        [img_to_array(array_to_img(im, scale=False).resize((32, 32))) for im in test_images]
    )




    return train_images, test_images


# ---------------- CREATE MODEL----------------------------

def create_model():
    ####### to investigate input_tensor
    # input_tensor = Input(shape=(32,32,3))

    model = tf.keras.Sequential([
        tf.keras.layers.Flatten(input_shape=(32, 32, 3)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(10)
    ])


    print('model created')
    return model


# ---------------- COMPILE MODEL----------------------------

def compile_model(learning_rate, model):
    optimiser_01 = Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimiser_01,
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    print('model compiled with learning rate', learning_rate)


# ---------------- TRAIN MODEL----------------------------

def train_model(train_images, train_labels, test_images, test_labels, model, batch_size, epochs):
    history = model.fit(train_images, train_labels,
                        epochs=epochs,
                        batch_size=batch_size,
                        validation_data=(test_images, test_labels))

    return history


# ---------------- PLOTTING RESULTS----------------------------

def plot_results(file_name, history, epochs, learning_rate, batch_size):
    # extracting accuracies and loss from history
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure()
    plt.title(f'epochs {epochs}, lr {learning_rate}, bs {batch_size}')

    # plotting accuracies
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Accuracy')
    plt.plot(epochs_range, val_acc, label='validation Accuracy')
    plt.xlabel('epochs')
    plt.ylabel('accuracy')
    plt.legend()

    # plotting losses
    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Loss')
    plt.plot(epochs_range, val_loss, label='validation Loss')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.savefig(f'{file_name}/plots/figure_E_{epochs}_LR_{learning_rate}_BS_{batch_size}.png')


def inference(model, test_data):
    # predict with model label of test data

    predictions = []
    for test_img in test_data:
        test_img = tf.expand_dims(test_img, 0)
        prediction = model.predict(test_img)
        prediction= np.argmax(prediction)
        predictions.append(prediction)
    return predictions


def plot_confusion_mat(predictions, test_labels):
    confusion_mtx = tf.math.confusion_matrix(test_labels, predictions)
    confmtx_fig = plt.figure(figsize=(10, 8))
    sns.heatmap(confusion_mtx, annot=True, fmt='g')
    plt.xlabel('Predicted label')
    plt.ylabel('True label')

    plt.savefig(f'resnet/confusion_plots/figure_E_{epochs}_LR_{learning_rate}_BS_{batch_size}.png')


def main(file_name, epochs, learning_rate, batch_size):

    print('######## RUNNING ', file_name, 'WITH LEARNING RATE ', learning_rate, ' BATCH SIZE', batch_size, ' and',
          epochs, ' EPOCHS')

    path = os.getcwd()  # getting current path

    # creating folder called resnet with subfolders histories (for history files) and plots (for all figures)
    if not os.path.exists(f'{path}/{file_name}'):
        dir_name = f"{path}/{file_name}/histories"
        os.makedirs(dir_name)
        dir_name = f"{path}/{file_name}/plots"
        os.mkdir(dir_name)
        dir_name = f"{path}/{file_name}/confusion_plots"
        os.mkdir(dir_name)

    # ---------------- LOADING DATA----------------------------

    # loading clothes images
    fashion_mnist = tf.keras.datasets.fashion_mnist

    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

    # names of classes:
    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                   'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

    # ---------------- PRE-PROCESSING DATA----------------------------
    train_images, test_images = preprocessing(train_images, test_images)


    # ---------------- CREATING FOLDER WHERE TO SAVE DATA----------------------------

    model = create_model()
    compile_model(learning_rate, model)
    history = train_model(train_images, train_labels, test_images, test_labels,  model, batch_size, epochs)

    # saving history
    with open(f'{file_name}/histories/history_E_{epochs}_LR_{learning_rate}_BS_{batch_size}', 'wb') as file_pi:
        pickle.dump(history.history, file_pi)


    plot_results(file_name, history, epochs,learning_rate, batch_size)

    predictions = inference(model, test_images)

    print(predictions)
    plot_confusion_mat(predictions, test_labels)
'''

if __name__ == '__main__':

    # getting arguments from input
    parsed_input = sys.argv
    if len(parsed_input) < 3:
        print('\nERROR: not enough input parameters. Please define a learning rate, batch size and number of epochs')
        exit()

    # file_name = parsed_input[0]
    learning_rate = float(parsed_input[1])
    epochs = int(parsed_input[2])
    batch_size = int(parsed_input[3])
    file_name ='normal'
    main(file_name, epochs, learning_rate, batch_size)
