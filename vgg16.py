
import sys

from common_functions import main

if __name__ == '__main__':

    # getting arguments from input
    parsed_input = sys.argv
    if len(parsed_input) < 3:
        print('\nERROR: not enough input parameters. Please define a learning rate, batch size and number of epochs')
        exit()

    # name_of_file = float(parsed_input[0])
    learning_rate = float(parsed_input[1])
    epochs = int(parsed_input[2])
    batch_size = int(parsed_input[3])

    file_name = 'vgg'
    main(file_name, epochs, learning_rate, batch_size)