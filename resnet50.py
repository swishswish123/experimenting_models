import os
import sys

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# Display
from IPython.display import Image, display
import matplotlib.cm as cm

from tensorflow.keras.preprocessing.image import img_to_array, array_to_img

from tensorflow.keras import Input
from tensorflow.keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16

from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input

from tensorflow.keras.optimizers import Adam

import pickle

import seaborn as sns

from common_functions import main

'''
# ---------------- PREPROCESSING----------------------------

def preprocessing(train_images, test_images):
    # 1. making the scale 0-1
    train_images = train_images / 255.0
    test_images = test_images / 255.0

    # 2. resizing images so we can use them with VGG model
    train_images=np.dstack([train_images]*3)
    test_images = np.dstack([test_images]*3)

    # reshaping to tensor format
    train_images = train_images.reshape([-1,28,28,3])
    test_images = test_images.reshape([-1, 28, 28, 3])

    train_images = np.asarray(
        [img_to_array(array_to_img(im, scale=False).resize((32,32))) for im in train_images]
    )

    test_images = np.asarray(
        [img_to_array(array_to_img(im, scale=False).resize((32,32))) for im in test_images]
    )

    return train_images, test_images

# ---------------- CREATE MODEL----------------------------

def create_model(file_name):

    ####### to investigate input_tensor
    # input_tensor = Input(shape=(32,32,3))

    resnet_model = tf.keras.applications.ResNet50(
        include_top=True,
        weights=None,
        input_shape=(32, 32, 3),
        pooling=None,
        classes=10,
        classifier_activation='softmax'
    )

    model = Model(resnet_model.inputs, resnet_model.outputs)

    print('model created')
    return model


# ---------------- COMPILE MODEL----------------------------

def compile_model(learning_rate, model):
    optimiser_01 = Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimiser_01, loss=tf.keras.losses.SparseCategoricalCrossentropy(),metrics=['accuracy'])
    print('model compiled with learning rate', learning_rate)


# ---------------- TRAIN MODEL----------------------------

def train_model(train_images, train_labels, test_images, test_labels,  model, batch_size, epochs):

    history = model.fit(train_images, train_labels,
                        epochs=epochs, batch_size=batch_size,
                        validation_data=(test_images, test_labels))

    return history

# ---------------- PLOTTING RESULTS----------------------------

def plot_results(file_name, history, epochs,learning_rate, batch_size):

    # extracting accuracies and loss from history
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure()
    plt.title(f'epochs {epochs}, lr {learning_rate}, bs {batch_size}')

    # plotting accuracies
    plt.subplot(1,2,1)
    plt.plot(epochs_range, acc, label='Accuracy')
    plt.plot(epochs_range, val_acc, label='validation Accuracy')
    plt.xlabel('epochs')
    plt.ylabel('accuracy')
    plt.legend()

    # plotting losses
    plt.subplot(1,2,2)
    plt.plot(epochs_range, loss, label='Loss')
    plt.plot(epochs_range, val_loss, label='validation Loss')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.savefig(f'{file_name}/plots/figure_E_{epochs}_LR_{learning_rate}_BS_{batch_size}.png')


def inference(model, test_data):
    # predict with model label of test data
    predictions = []
    for test_img in test_data:
        test_img = tf.expand_dims(test_img, 0)
        prediction = model.predict(test_img)

        predictions.append(prediction)
    return predictions

def plot_confusion_mat(predictions, test_labels):
    confusion_mtx = tf.math.confusion_matrix(test_labels, predictions)
    confmtx_fig = plt.figure(figsize=(10, 8))
    sns.heatmap(confusion_mtx, annot=True, fmt='g')
    plt.xlabel('Predicted label')
    plt.ylabel('True label')

    plt.savefig(f'resnet/confusion_plots/figure_E_{epochs}_LR_{learning_rate}_BS_{batch_size}.png')

def get_img_array(img_path, size):
    # `img` is a PIL image of size 299x299
    img = tf.keras.preprocessing.image.load_img(img_path, target_size=size)
    # `array` is a float32 Numpy array of shape (299, 299, 3)
    array = tf.keras.preprocessing.image.img_to_array(img)
    # We add a dimension to transform our array into a "batch"
    # of size (1, 299, 299, 3)
    array = np.expand_dims(array, axis=0)
    return array


def make_gradcam_heatmap(img_array, model, last_conv_layer_name, pred_index=None):
    # First, we create a model that maps the input image to the activations
    # of the last conv layer as well as the output predictions
    grad_model = tf.keras.models.Model(
        [model.inputs], [model.get_layer(last_conv_layer_name).output, model.output]
    )

    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        last_conv_layer_output, preds = grad_model(img_array)
        if pred_index is None:
            pred_index = tf.argmax(preds[0])
        class_channel = preds[:, pred_index]

    # This is the gradient of the output neuron (top predicted or chosen)
    # with regard to the output feature map of the last conv layer
    grads = tape.gradient(class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    # then sum all the channels to obtain the heatmap class activation
    last_conv_layer_output = last_conv_layer_output[0]
    heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
    heatmap = tf.squeeze(heatmap)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
    return heatmap.numpy()


def main(file_name, epochs, learning_rate, batch_size):

    print('######## LEARNING RATE ',learning_rate, ' BATCH SIZE', batch_size, ' and', epochs, ' EPOCHS')

    path = os.getcwd() # getting current path

    # creating folder called resnet with subfolders histories (for history files) and plots (for all figures)
    if not os.path.exists(f'{path}/resnet'):
        dir_name = f"{path}/{file_name}/histories"
        os.makedirs(dir_name)
        dir_name = f"{path}/{file_name}/plots"
        os.mkdir(dir_name)
        dir_name = f"{path}/{file_name}/confusion_plots"
        os.mkdir(dir_name)


    # ---------------- LOADING DATA----------------------------

    # loading clothes images
    fashion_mnist = tf.keras.datasets.fashion_mnist

    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

    # names of classes:
    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                   'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

    # ---------------- PRE-PROCESSING DATA----------------------------

    train_images, test_images =  preprocessing(train_images, test_images)

    # ---------------- CREATING FOLDER WHERE TO SAVE DATA----------------------------

    model = create_model(file_name)
    compile_model(learning_rate, model)
    history = train_model(train_images, train_labels, test_images, test_labels,  model, batch_size, epochs)

    # saving history
    with open(f'{file_name}/histories/history_E_{epochs}_LR_{learning_rate}_BS_{batch_size}', 'wb') as file_pi:
        pickle.dump(history.history, file_pi)

    plot_results(file_name,history, epochs,learning_rate, batch_size)

    predictions = inference(model, test_images)
    print('### predictions:', predictions)

    plot_confusion_mat(predictions, test_labels)
'''

if __name__ == '__main__':

    # getting arguments from input
    parsed_input = sys.argv
    if len(parsed_input) < 3:
        print('\nERROR: not enough input parameters. Please define a learning rate, batch size and number of epochs')
        exit()

    # name_of_file = float(parsed_input[0])
    learning_rate = float(parsed_input[1])
    epochs = int(parsed_input[2])
    batch_size = int(parsed_input[3])

    file_name = 'resnet'
    main(file_name,epochs, learning_rate, batch_size)